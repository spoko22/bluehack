package com.bluehack;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;


import com.bluehack.http.BluehackRestService;
import com.bluehack.http.GetMenuRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.CaptureActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.rest.RestService;

@EActivity(R.layout.content_well_met)
public class WellMet extends CaptureActivity {

    @Click(R.id.buttonScan)
    void onClick(){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setPrompt("Zrób zdjęcie QR kodu menu!");
        integrator.initiateScan();
    }

    @AfterViews
    void bindSnack(){

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setPrompt("Zrób zdjęcie QR kodu menu!");
        integrator.initiateScan();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            Toast.makeText(WellMet.this, contents, Toast.LENGTH_LONG).show();
            // Handle successful scan
            Intent intent2 = new Intent(this, DrawerActivityV2_.class);
//            finish();
            WellMet.this.finish();
            startActivity(intent2);



        } else if (resultCode == RESULT_CANCELED) {
            // Handle cancel
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            WellMet.this.finish();
            System.exit(0);
        }
        return super.onKeyDown(keyCode, event);
    }

}