package com.bluehack;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.bluehack.fragments.CallbackFragments;
import com.bluehack.fragments.CartKeeperFragment;
import com.bluehack.fragments.CartKeeperFragment_;
import com.bluehack.fragments.MenuContentFragment;
import com.bluehack.fragments.MenuContentFragment_;
import com.bluehack.http.BluehackRestService;
import com.bluehack.http.GetMenuRequest;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;

@EActivity
public class DrawerActivityV2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CallbackFragments {

    private Cart cart;

    @RestService
    BluehackRestService bluehackRestService;
    private RestaurantMenu menu;
    ProgressDialog progressDialog;

    private CartKeeperFragment cartKeeperFragment;
    private MenuContentFragment mainsFragment;

    @Pref
    SharedPrefs_ prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_activity_v2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @AfterViews
    void addMenu(){
        cart = new Cart();

        cartKeeperFragment = new CartKeeperFragment_();
        cartKeeperFragment.setCart(cart);
        addFragment(cartKeeperFragment, "cart");

        mainsFragment = new MenuContentFragment_();
        mainsFragment.setCart(cart);
        addFragment(mainsFragment, "meals");

        MyTask task = new MyTask(this);
        task.execute();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStackImmediate();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer_activity_v2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.basket) {
            addFragment(cartKeeperFragment, "cart");

        } else if (id == R.id.soup) {
            addFragment(mainsFragment, "meals");
            mainsFragment.getAdapter().setData(menu.getMenu().getSoups());
            mainsFragment.getAdapter().notifyDataSetChanged();
        } else if (id == R.id.main_dish) {
            addFragment(mainsFragment, "meals");
            mainsFragment.getAdapter().setData(menu.getMenu().getMains());
        } else if (id == R.id.dessert) {
            addFragment(mainsFragment, "meals");
            mainsFragment.getAdapter().setData(menu.getMenu().getDesserts());
        } else if (id == R.id.drink) {
            addFragment(mainsFragment, "meals");
            mainsFragment.getAdapter().setData(menu.getMenu().getSodas());
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addFragment(Fragment fragment, String tag){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentPlaceholder, fragment).addToBackStack("frag");
        transaction.commit();
        getFragmentManager().executePendingTransactions();
    }

    @Override
    public void changeDataSet(ArrayList<Dish> dishes) {
        mainsFragment.getAdapter().setData(dishes);
    }

    private class MyTask extends AsyncTask<Void, Void, Void> {

        CallbackFragments callbackFragments;

        @Override
        protected void onPreExecute() {
            publishProgressTo(true);
        }

        public MyTask(CallbackFragments callbackFragments) {
            super();
            this.callbackFragments = callbackFragments;
        }

        @Override
        protected Void doInBackground(Void... params) {

            GetMenuRequest request1 = new GetMenuRequest();
            request1.setRestaurantName("mmmklasyka");
            request1.setTableNo("1");
            menu = bluehackRestService.getMenu(request1);
            publishProgressTo(false);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            callbackFragments.changeDataSet(menu.getMenu().getDesserts());
        }
    }
    void publishProgressTo(boolean progress) {
        if (progress) {
            progressDialog = ProgressDialog.show(DrawerActivityV2.this, "Logging",
                    "Processing please wait...", true);
        } else {
            progressDialog.dismiss();
        }
    }


}
