package com.bluehack.fragments;

import com.bluehack.Dish;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ragnar on 10/25/15.
 */
public interface CallbackFragments {
    void changeDataSet(ArrayList<Dish> dishes);
}
