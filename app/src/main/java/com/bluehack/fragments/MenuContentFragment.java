package com.bluehack.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluehack.Cart;
import com.bluehack.CartModifier;
import com.bluehack.Dish;
import com.bluehack.R;
import com.bluehack.http.BluehackRestService;
import com.bluehack.tools.DividerItemDecoration;
import com.michael.easydialog.EasyDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 2015-10-24.
 */

@EFragment(R.layout.fragment_menu_content)
public class MenuContentFragment extends Fragment {
    private Cart cart;
    private MyAdapter adapter;

    public MyAdapter getAdapter() {
        return adapter;
    }

    @RestService
    BluehackRestService bluehackRestService;
    ProgressDialog progressDialog;
    ArrayList<Dish> list;

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    void setAdapter(){
        List<Dish> dishList = new ArrayList<>();
        adapter = new MyAdapter(dishList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

    }

    public void setCart(Cart cart){this.cart = cart;}

    public class MyAdapter extends RecyclerView.Adapter<MenuContentFragment.ViewHolder> implements CartModifier {
        List<Dish> dataSet;

        public MyAdapter(List<Dish> dataSet){
            this.dataSet = dataSet;
        }

        public void setData(List<Dish> dataSet){
            this.dataSet = dataSet;
            notifyDataSetChanged();
        }

        @Override
        public MenuContentFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menu_list_item, parent, false);
            return new ViewHolder(v, this);
        }

        @Override
        public void onBindViewHolder(MenuContentFragment.ViewHolder holder, int position) {
            Dish dish = dataSet.get(position);
            holder.firstLine.setText(dish.getName());
            String price = String.valueOf(dish.getPrice()) + "zł";
            holder.secondLine.setText(price);
            String filename = dish.getFilename();
            Drawable drawable = getResources().getDrawable(getResources().getIdentifier(filename,"drawable","com.bluehack"));
            holder.thumbnail.setImageResource(getResources().getIdentifier(filename, "drawable", "com.bluehack"));
            //TODO  holder.icon ... ustawienie ikonki na podstawie obiektu dish
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }

        @Override
        public void modifyCartContent(int adapterPosition) {
            cart.addDish(dataSet.get(adapterPosition));
            showSnackbar("Dodano do koszyka: " + dataSet.get(adapterPosition).getName());
        }

        @Override
        public void showDialogTip(View view, boolean inverted, int adapterPosition){
            Dish dish = dataSet.get(adapterPosition);
            showTip(view, inverted, dish);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder{
        private CartModifier cartModifier;
        TextView firstLine;
        TextView secondLine;
        ImageView thumbnail;
        ImageView addToCart;

        public ViewHolder(View itemView, CartModifier callback) {
            super(itemView);
            this.cartModifier = callback;
            firstLine = (TextView) itemView.findViewById(R.id.firstLine);
            secondLine = (TextView) itemView.findViewById(R.id.priceValue);
            addToCart = (ImageView) itemView.findViewById(R.id.addToCart);
            thumbnail = (ImageView) itemView.findViewById(R.id.icon);

            addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartModifier.modifyCartContent(getAdapterPosition());
                    Log.d("Menu", "Added to cart: " + getAdapterPosition());
                }
            });

            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean inverted = false;
                    if(getLayoutPosition() > 3) inverted = true;
                    cartModifier.showDialogTip(v, inverted, getAdapterPosition());
                }
            });
        }

    }

    private void showSnackbar(String text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .show();
    }

    private void showTip(View v, boolean inverted, Dish dish){
        View view = getActivity().getLayoutInflater().inflate(R.layout.layout_tip_content_horizontal, null);
        ImageView picture = (ImageView) view.findViewById(R.id.dishIcon);
        TextView description = (TextView) view.findViewById(R.id.dishDesc);
        TextView ingredients = (TextView) view.findViewById(R.id.dishIngr);
        ArrayList<String> skladniki = dish.getIngredients();
        String filename = dish.getFilename();
        String descr = dish.getDescription();
        String ingre = "Składniki: ";
        for (int i = 0; i < skladniki.size(); i++){
            if (i == skladniki.size() - 1){
                ingre = ingre + skladniki.get(i);
            }else {
                ingre = ingre + skladniki.get(i) + ", ";
            }
        }
        picture.setImageResource(getResources().getIdentifier(filename,"drawable","com.bluehack"));
        description.setText(descr);
        ingredients.setText(ingre);
        //TODO tu nalezy podstawic opis i fotke i bedzie gites
        EasyDialog dialog = new EasyDialog(getActivity())
                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
                .setLayout(view)
                .setBackgroundColor(getActivity().getResources().getColor(R.color.background_color_black))
                        // .setLocation(new location[])//point in screen
                .setLocationByAttachedView(v)
                .setGravity(EasyDialog.GRAVITY_BOTTOM)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24);
                //.setOutsideColor(MainActivity.this.getResources().getColor(R.color.outside_color_trans))

        if(inverted) dialog.setGravity(EasyDialog.GRAVITY_TOP);
        dialog.show();
    }

}
