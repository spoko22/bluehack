package com.bluehack.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluehack.Cart;
import com.bluehack.CartModifier;
import com.bluehack.Dish;
import com.bluehack.R;
import com.bluehack.http.BluehackRestService;
import com.bluehack.http.PostOrder;
import com.bluehack.http.PostOrderResponse;
import com.bluehack.tools.DividerItemDecoration;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal on 2015-10-24.
 */

@EFragment(R.layout.fragment_booty_keeper)
public class CartKeeperFragment extends Fragment {
    private Cart cart;
    PostOrderResponse response;
    @RestService
    BluehackRestService bluehackRestService;
    @ViewById(R.id.cartContentRV)
    RecyclerView recyclerView;
    @ViewById(R.id.cartText)
    TextView cartText;
    @ViewById(R.id.totalPriceAmount)
    TextView totalPriceAmount;
    @ViewById(R.id.payButton)
    Button payButton;

    ProgressDialog progressDialog;
    MyAdapter adapter;

    @AfterViews
    void bindAdapter(){
        adapter = new MyAdapter(new ArrayList<Cart.Order>(cart.getCartContent().values()));
        if(adapter.getItemCount() == 0) cartText.setText(R.string.cart_content_empty);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
    }
    @Click(R.id.payButton)
    public void orderRequest(){
        if (adapter.getItemCount() == 0) {
            cartText.setText(R.string.cart_content_empty);
            Snackbar snackbar = Snackbar.make(getView(),"Brak pozycji!",Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            MyTask task = new MyTask();
            task.execute();
            int i = cart.getCartContent().size();
            adapter.setData(new ArrayList<Cart.Order>());
            cartText.setText(R.string.cart_content_empty);
            updatePrice();
            totalPriceAmount.setText(" 0zł");
        }
    };

    private class MyTask extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            publishProgressTo(true);
        }
        @Override
        protected Void doInBackground(Void... params) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(2000);
//                        publishProgressTo(false);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }).start();
            PostOrder order = new PostOrder();

            String price = Integer.toString(cart.getCartValue())+" zl";
            StringBuilder sb = new StringBuilder("Zamówienie: ");
            for (Map.Entry<String, Cart.Order> entry : cart.getCartContent().entrySet()){
                sb.append(entry.getValue().getDish().getName()).append(" ").append("(").append(Integer.toString(entry.getValue().getCount())).append("), ");
            }

            order.setPrice(price);
            order.setOrder(sb.toString());
            response = bluehackRestService.postOrder(order);

            return null;
        }

        void publishProgressTo(boolean progress) {
            if (progress) {
                progressDialog = ProgressDialog.show(getActivity(), "Wysyłanie zamówienia",
                        "Proszę czekać", true);
            } else {
                progressDialog.dismiss();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            publishProgressTo(false);
            if(response.getResult().equals("ok")){
                showSnackbar("Kucharz "+response.getWaiterName()+" przygotowuje dla ciebie jedzonko");
            }else{
                showSnackbar(":(");
            }

        }
    }
    public void setCart(Cart cart){
        this.cart = cart;
    }

    private void showSnackbar(String text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG)
                .show();
    }

    private class MyAdapter extends RecyclerView.Adapter<CartKeeperFragment.ViewHolder> implements CartModifier {
        List<Cart.Order> dataSet;

        public MyAdapter(List<Cart.Order> dataSet){
            this.dataSet = dataSet;
            updatePrice();
        };

        public void clearData(){
            dataSet.clear();
        }

        public void setData(List<Cart.Order> dataSet){
            this.dataSet = dataSet;
            notifyDataSetChanged();
            updatePrice();
        }

        @Override
        public CartKeeperFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cart_content_list_item, parent, false);
            return new ViewHolder(v, this);
        }

        @Override
        public void onBindViewHolder(CartKeeperFragment.ViewHolder holder, int position) {
            Cart.Order order = dataSet.get(position);
            Dish dish = order.getDish();
            holder.firstLine.setText(order.getDish().getName());
            holder.amountValue.setText(String.valueOf(order.getCount()));
            String price = String.valueOf(order.getDish().getPrice() * order.getCount()) + "zł";
            holder.priceValue.setText(price);
            String filename = dish.getFilename();
            holder.thumbnail.setImageResource(getResources().getIdentifier(filename,"drawable","com.bluehack"));
            //TODO  holder.icon ... ustawienie ikonki na podstawie obiektu dish
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }

        @Override
        public void modifyCartContent(int adapterPosition) {
            cart.getCartContent().remove(dataSet.get(adapterPosition).getDish().getName());
            dataSet.remove(adapterPosition);
            notifyItemRemoved(adapterPosition);
            if(dataSet.size() == 0) cartText.setText(R.string.cart_content_empty);
            updatePrice();
        }

        @Override
        public void showDialogTip(View v, boolean inverted, int adapterPosition) {
            //tutaj tego chyba nie potrzebujemy
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder{
        private CartModifier cartModifier;
        TextView firstLine;
        TextView amountValue;
        TextView priceValue;
        ImageView thumbnail;
        ImageView removeFromCart;

        public ViewHolder(View itemView, CartModifier callback) {
            super(itemView);
            this.cartModifier = callback;
            firstLine = (TextView) itemView.findViewById(R.id.firstLine);
            amountValue = (TextView) itemView.findViewById(R.id.amountValue);
            priceValue = (TextView) itemView.findViewById(R.id.priceValue);
            removeFromCart = (ImageView) itemView.findViewById(R.id.removeFromCart);
            thumbnail = (ImageView) itemView.findViewById(R.id.icon);
            removeFromCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartModifier.modifyCartContent(getAdapterPosition());
                    Log.d("Menu", "Removed from cart: " + getAdapterPosition());
                }
            });
        }
    }

    private void updatePrice(){
        totalPriceAmount.setText(" " + String.valueOf(cart.getCartValue())+"zł");
    }

}
