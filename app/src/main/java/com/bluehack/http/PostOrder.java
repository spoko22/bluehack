package com.bluehack.http;

import com.bluehack.Cart;

import org.androidannotations.annotations.rest.Post;

import java.util.Map;

/**
 * Created by ragnar on 10/25/15.
 */
public class PostOrder {

    private String order;
    private String price;

    public PostOrder(){
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
