package com.bluehack.http;

/**
 * Created by ragnar on 10/24/15.
 */
public class GetMenuRequest {

    private String restaurantName;
    private String tableNo;

    public GetMenuRequest() {
    }

    public String getTableNo() {
        return tableNo;
    }

    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
