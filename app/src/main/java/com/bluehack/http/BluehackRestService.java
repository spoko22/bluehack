package com.bluehack.http;

import com.bluehack.Dish;
import com.bluehack.RestaurantMenu;


import org.androidannotations.annotations.rest.Post;

import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;

/**
 * Created by ragnar on 10/24/15.
 */

@Rest(rootUrl = "http://192.168.1.108:3000/api", converters = {MappingJackson2HttpMessageConverter.class})
public interface BluehackRestService {

    @Post("/getMenu")
    RestaurantMenu getMenu(GetMenuRequest getMenuRequest);

    @Post("/getSoups")
    ArrayList<Dish> getSoups(GetMenuRequest getMenuRequest);

    @Post("/getMains")
    ArrayList<Dish> getMains(GetMenuRequest getMenuRequest);

    @Post("/getSodas")
    ArrayList<Dish> getSodas(GetMenuRequest getMenuRequest);

    @Post("/getDesserts")
    ArrayList<Dish> getDessert(GetMenuRequest getMenuRequest);

    @Post("/getTest")
    GetTestResponse getTest(GetTestRequest getTestRequest);

    @Post("/postOrder")
    PostOrderResponse postOrder(PostOrder postOrder);
}
