package com.bluehack.http;

/**
 * Created by ragnar on 10/24/15.
 */
public class GetTestRequest {

    private String userInput;

    public GetTestRequest() {
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }
}
