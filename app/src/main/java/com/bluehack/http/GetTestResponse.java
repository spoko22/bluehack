package com.bluehack.http;

/**
 * Created by ragnar on 10/24/15.
 */
public class GetTestResponse {

    private String userInput;
    private String result;

    public GetTestResponse() {

    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    @Override
    public String toString() {
        return "GetTestResponse{" +
                "userInput='" + userInput + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
