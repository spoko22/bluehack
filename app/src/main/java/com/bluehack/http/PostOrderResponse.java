package com.bluehack.http;

/**
 * Created by ragnar on 10/25/15.
 */
public class PostOrderResponse {

    private String result;
    private String waiterName;

    public PostOrderResponse() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getWaiterName() {
        return waiterName;
    }

    public void setWaiterName(String waiterName) {
        this.waiterName = waiterName;
    }
}
