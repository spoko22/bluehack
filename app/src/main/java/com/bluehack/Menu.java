package com.bluehack;

import java.util.ArrayList;

/**
 * Created by ragnar on 10/24/15.
 */
public class Menu {

    private ArrayList<Dish> soups;
    private ArrayList<Dish> mains;
    private ArrayList<Dish> sodas;
    private ArrayList<Dish> desserts;

    public ArrayList<Dish> getSoups() {
        return soups;
    }

    public void setSoups(ArrayList<Dish> soups) {
        this.soups = soups;
    }

    public ArrayList<Dish> getMains() {
        return mains;
    }

    public void setMains(ArrayList<Dish> mains) {
        this.mains = mains;
    }

    public ArrayList<Dish> getSodas() {
        return sodas;
    }

    public void setSodas(ArrayList<Dish> sodas) {
        this.sodas = sodas;
    }

    public ArrayList<Dish> getDesserts() {
        return desserts;
    }

    public void setDesserts(ArrayList<Dish> desserts) {
        this.desserts = desserts;
    }

    public Menu() {
    }


}
