package com.bluehack;

/**
 * Created by Michal on 2015-10-24.
 */
public interface NavigationDrawerCallback{
    void onNavigationDrawerItemClick(int position);
}