package com.bluehack;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Michal on 2015-10-24.
 */
@SharedPref
public interface SharedPrefs {
    @DefaultString("key")
    String key();

    @DefaultString("value")
    String value();
}
