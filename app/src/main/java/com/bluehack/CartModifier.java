package com.bluehack;

import android.view.View;

/**
 * Created by Michal on 2015-10-24.
 */
public interface CartModifier {
    void modifyCartContent(int adapterPosition);
    void showDialogTip(View v, boolean inverted, int adapterPosition);
}
