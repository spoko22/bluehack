package com.bluehack;

import java.util.ArrayList;
/**
 * @author Bartłomiej Borucki
 */
public class Dish {
    /**
     * Nazwa potrawy
     */
    private String name;

    /**
     * Cena potrawy
     */
    private int price;

    /**
     * Flaga dostępności
     */
    private boolean available;

    /**
     * Lista składników
     */
    private ArrayList<String> ingredients;

    /**
     * Opis potrawy
     */
    private String description;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * nazwa obrazka potrawy
     */
    private String filename;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
