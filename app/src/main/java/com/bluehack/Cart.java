package com.bluehack;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michal on 2015-10-24.
 */
public class Cart {
    private Map<String, Order> cartContent;

    public int getCartValue(){
        int sum = 0;
        for(Map.Entry<String, Order> entry : cartContent.entrySet()){
            sum += entry.getValue().getCount() * entry.getValue().getDish().getPrice();
        }
        return sum;
    }

    public Cart(){
        this.cartContent = new HashMap<>();
    }

    public void clearContent(){
        cartContent.clear();
    }

    public void addDish(Dish dish){
        Order order = cartContent.get(dish.getName());
        if(order != null) order.increaseCount(1);
        else{
            order = new Order(dish);
            cartContent.put(dish.getName(), order);
        }
    }

    public Map<String, Order> getCartContent() {
        return cartContent;
    }

    public void clearOrder(){
        cartContent.clear();
    }

    public class Order{
        private Dish dish;
        private int count;

        public Order(Dish dish){
            this.dish = dish;
            count = 1;
        }

        public Dish getDish(){return dish;}

        public void increaseCount(int toAdd){
            this.count += toAdd;
        }

        public void decreaseCount(int toRemove){
            this.count -= toRemove;
        }

        public int getCount(){ return count;}
        public int getOrderValue(){ return count * dish.getPrice();}
    }
}
