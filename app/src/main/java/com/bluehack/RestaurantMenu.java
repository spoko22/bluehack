package com.bluehack;

import java.util.ArrayList;

/**
 * Created by ragnar on 10/24/15.
 */
public class RestaurantMenu {

    private String name;
    private String location;
    private Menu menu;

    public RestaurantMenu() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
